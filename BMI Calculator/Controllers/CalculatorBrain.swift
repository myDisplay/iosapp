//
//  CalculatorBrain.swift
//  BMI Calculator
//
//  Created by Aravind Balaji on 20/08/20.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import UIKit

struct CalculatorBrain{
    var bmi : BMI?
    
    var bmiValue : Float?
    
    func getBMIValue() -> String {
        return String(format: "%.1f", bmi?.value ?? 0.0)
    }
    
    func getColor() -> UIColor{
        return bmi?.color ?? UIColor.green
    }
    
    func getAdvice() -> String{
        return bmi?.advice ?? "NO Advice"
    }
    
    mutating func calculateBMI(height : Float, weight : Float) {
        bmiValue = Float(weight) / pow(height, 2)
        
        
        if(bmiValue! < 18.5){
            bmi = BMI(value: bmiValue!, advice: "Eat More", color: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))
        } else if (bmiValue! < 24.9){
            bmi = BMI(value: bmiValue!, advice: "All Good", color: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1))
        } else {
            bmi = BMI(value: bmiValue!, advice: "Eat Less", color: #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1))
        }
        
    }
    
    
}
