//
//  ResultViewController.swift
//  BMI Calculator
//
//  Created by Aravind Balaji on 18/08/20.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    var bmiValueToSet : String?
    var advice : String?
    var color : UIColor?
    
    
    @IBOutlet weak var bmiValueLbl: UILabel!
    
    @IBOutlet weak var adviceLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if bmiValueToSet != nil {
            print("bmiValueToSet"+bmiValueToSet!)
            bmiValueLbl.text = bmiValueToSet
            adviceLbl.text = advice
            self.view.backgroundColor = color
        }
        
        
    }
    
    @IBAction func recalculatePressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

}
