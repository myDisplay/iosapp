//
//  ViewController.swift
//  BMI Calculator
//
//  Created by Angela Yu on 21/08/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit

class calculateViewController: UIViewController {

    @IBOutlet weak var heightValue: UILabel!
    @IBOutlet weak var weightValue: UILabel!
    
    
    @IBOutlet weak var weightOutlet: UISlider!
    @IBOutlet weak var heightOutlet: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func weightSlider(_ sender: UISlider) {
        
        let currValue = Int(sender.value)
        weightValue.text = "\(currValue)kg"
    }
    @IBAction func heightSlider(_ sender: UISlider) {
        let senValue = sender.value
        let currentValue = String(format: "%.2f", senValue)
        heightValue.text = currentValue+"m"
    }
    @IBAction func calculatePressed(_ sender: UIButton) {
        let heightValue = heightOutlet.value
        let weightValue = weightOutlet.value
        
        let bmi = Float(weightValue) / pow(heightValue, 2)
        let alert = UIAlertController(title: "", message: String(format:"%0.2f", bmi), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }

}

