//
//  ViewController.swift
//  BMI Calculator
//
//  Created by Angela Yu on 21/08/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit

class calculateViewController: UIViewController {

    var controlBrain = CalculatorBrain()
    @IBOutlet weak var heightValue: UILabel!
    @IBOutlet weak var weightValue: UILabel!
    
    
    @IBOutlet weak var weightOutlet: UISlider!
    @IBOutlet weak var heightOutlet: UISlider!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func weightSlider(_ sender: UISlider) {
        
        let currValue = Int(sender.value)
        weightValue.text = "\(currValue)kg"
    }
    @IBAction func heightSlider(_ sender: UISlider) {
        let senValue = sender.value
        let currentValue = String(format: "%.2f", senValue)
        heightValue.text = currentValue+"m"
    }
    @IBAction func calculatePressed(_ sender: UIButton) {
        let heightValue = heightOutlet.value
        let weightValue = weightOutlet.value
        
        var calcBMI = controlBrain.calculateBMI(height: heightValue, weight: weightValue)
        
        //self.performSegue(withIdentifier: "navigateToResultScreen", sender: self)
        /*
        let alert = UIAlertController(title: "", message: String(format:"%0.2f", bmi), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
          */
        //let secondVC = ResultViewController()
        
        
        let secondVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "secondScreen") as! ResultViewController
        secondVC.bmiValueToSet = controlBrain.getBMIValue()
        secondVC.advice = controlBrain.getAdvice()
        secondVC.color = controlBrain.getColor()
        self.present(secondVC, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "navigateToResultScreen" {
            let destController = segue.destination as! ResultViewController
            //destController.bmiValueToSet = controlBrain.bmiValue
        }
    }
    

}

